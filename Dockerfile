FROM node:16.15 as dev

WORKDIR /app

VOLUME /app

CMD ["npm", "start"]

FROM node:16.15 as build

WORKDIR /app

COPY package*.json ./

RUN npm install --location=global npm@8.13.2 \
    && npm ci --omit=dev --ignore-scripts

COPY public public
COPY src src

# mock CI environment to have a real prod build
RUN CI=true npm run build

FROM nginx:1.22.0-alpine

LABEL \
    org.label-schema.dockerfile="/Dockerfile" \
    org.label-schema.license="AGPL-3.0-or-later" \
    org.label-schema.maintainer="Stefan Hynek" \
    org.label-schema.name="WebLab" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://gitlab.gwdg.de/dariah-de/weblab" \
    org.label-schema.vcs-url="https://gitlab.gwdg.de/dariah-de/weblab.git" \
    org.label-schema.vendor="SUB/FE"

COPY --from=build /app/build /usr/share/nginx/html

ARG build_date
ARG vcs_ref
ARG version
LABEL \
    org.label-schema.build-date="${build_date}" \
    org.label-schema.vcs-ref="${vcs_ref}" \
    org.label-schema.version="${version}"

COPY Dockerfile /
