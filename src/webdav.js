const { createClient } = require("webdav");

// Get directory contents
export default async function directoryItems(auth, path) {
  const client = createClient("/repdav", {
    username: auth,
    password: "",
  });
  let dI;
  if (path) {
    dI = await client.getDirectoryContents(path);
  } else {
    dI = await client.getDirectoryContents("/");
  }
  console.log(dI);
  return dI;
}
