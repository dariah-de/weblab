import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  dsn: "https://ddc2d86d530d4bcaab0cb08feb3fefc2@errs.sub.uni-goettingen.de/12",
  environment: process.env.NODE_ENV,
  integrations: [new Integrations.BrowserTracing()],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

if (process.env.NODE_ENV === "development")
  console.debug(process.env);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
