import { LoadingButton } from "@mui/lab";
import {
  Button,
  ButtonGroup,
  Checkbox,
  Container,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  Link as MuiLink,
  MenuItem,
  Select,
  Stack,
  Typography,
} from "@mui/material";
import SaveIcon from "@mui/icons-material/Save";
import BugReportIcon from "@mui/icons-material/BugReport";
import ExploreIcon from "@mui/icons-material/Explore";
import SettingsIcon from "@mui/icons-material/Settings";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { AppContext } from "../AppContext";

// info: [source code](https://gitlab.gwdg.de/dariah-de/textgridrep/textgrid-authentication-and-authorization/-/blob/main/info.textgrid.middleware.tgauth.webauth/secure/TextGrid-WebAuth-GetSid.php)

/**
 * Hint: Component names (e.g. icon) are PascalCase
 * slugs are kebab-case and MUST be unique
 * labels should be subject to I18N
 * alwaysEnabled is boolean
 * TODOs:
 *   - outsource into a config module together with textgrid service classes? at least somewhere in another file, maybe within the settings folder
 */
export const plugins = [
  {
    order: 99,
    slug: "settings",
    label: "Settings",
    alwaysEnabled: true,
    icon: SettingsIcon,
  },
  {
    order: 1,
    slug: "explorer",
    label: "Explorer",
    alwaysEnabled: false,
    icon: ExploreIcon,
  },
  {
    order: 2,
    slug: "roger",
    label: "ROGER",
    alwaysEnabled: false,
    icon: BugReportIcon,
  },
];

export default function Settings() {
  const [appContext, setAppContext] = useContext(AppContext);
  const [selectedPlugins, setSelectedPlugins] = React.useState(
    appContext.plugins
  );

  // TODO: make the redirect somehow dynamic depending on the deployment; maybe with a env var that sets the hostname
  const textgridServiceClasses = [
    {
      id: 0,
      slug: "test",
      label: "Testing",
      host: "test.textgridlab.org",
      url:
        process.env.REACT_APP_TGSID === undefined
          ? "https://test.textgridlab.org/1.0/secure/TextGrid-WebAuth-GetSid.php?authZinstance=https://test.textgridlab.org&return=https://141.5.109.204:3000/getsid&type=hash"
          : `/getsid/#${process.env.REACT_APP_TGSID}`,
    },
    {
      id: 1,
      slug: "stag",
      label: "Staging",
      host: "dev.textgridlab.org",
      url: "https://dev.textgridlab.org/1.0/secure/TextGrid-WebAuth-GetSid.php?authZinstance=textgrid-esx1.gwdg.de&return=https://141.5.109.204:3000/getsid&type=hash",
    },
    {
      id: 2,
      slug: "prod",
      label: "Production",
      host: "textgridlab.org",
      url: "https://textgridlab.org/1.0/secure/TextGrid-WebAuth-GetSid.php?authZinstance=textgrid-esx2.gwdg.de&return=https://141.5.109.204:3000/getsid&type=hash",
    },
  ];

  const handleTextgridServiceClassChange = (event) => {
    setAppContext((context) => ({
      ...context,
      textgridServiceClass: event.target.value,
    }));
  };

  /** handle the selection on plugin checkboxes as a local component state */
  const handlePluginChange = (event) => {
    setSelectedPlugins(
      event.target.checked
        ? selectedPlugins.concat([event.target.name])
        : selectedPlugins.filter((p) => p !== event.target.name)
    );
  };

  /**
   * save the selection of plugins to the appContext
   */
  const handlePluginSubmit = (event) => {
    //if preventDefault is used, the sidebar is not refreshed! => do it manually on change of appContext in SidePanel
    event.preventDefault();
    setAppContext((context) => ({
      ...context,
      plugins: selectedPlugins,
    }));
  };

  return (
    <Container>
      <Typography component="h2" variant="h2">
        Plugins
      </Typography>
      <FormGroup>
        {plugins.map((p) => (
          <FormControlLabel
            checked={p.alwaysEnabled ? true : selectedPlugins.includes(p.slug)}
            control={<Checkbox />}
            disabled={p.alwaysEnabled}
            key={p.slug}
            label={p.label}
            name={p.slug}
            onChange={handlePluginChange}
          />
        ))}
        <ButtonGroup>
          <LoadingButton
            /* loading */
            loadingPosition="start"
            onClick={handlePluginSubmit}
            startIcon={<SaveIcon />}
            type="submit"
            variant="outlined"
          >
            Submit
          </LoadingButton>
        </ButtonGroup>
      </FormGroup>
      <Typography component="h2" variant="h2">
        Services
      </Typography>
      <Typography component="h3" variant="h3">
        Textgrid
      </Typography>
      Connected to{" "}
      {textgridServiceClasses[appContext.textgridServiceClass].label} (
      {textgridServiceClasses[appContext.textgridServiceClass].host}).
      <Stack direction="row" spacing={2}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-label">Service class</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={appContext.textgridServiceClass}
            label="Age"
            onChange={handleTextgridServiceClassChange}
          >
            {textgridServiceClasses.map((tgClass) => (
              <MenuItem key={tgClass.id} value={tgClass.id}>
                {tgClass.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        {
          // for external references use MuiLink instead of Link component from Router
          textgridServiceClasses
            .find(
              (textgridServiceClass) =>
                textgridServiceClass.id === appContext.textgridServiceClass
            )
            .url.startsWith("http") ? (
              <Button
                component={MuiLink}
                href={
                  textgridServiceClasses.find(
                    (textgridServiceClass) =>
                      textgridServiceClass.id === appContext.textgridServiceClass
                  ).url
                }
              >
              Connect
              </Button>
            ) : (
              <Button
                component={Link}
                to={
                  textgridServiceClasses.find(
                    (textgridServiceClass) =>
                      textgridServiceClass.id === appContext.textgridServiceClass
                  ).url
                }
              >
              Connect
              </Button>
            )
        }
      </Stack>
    </Container>
  );
}
