import { Breadcrumbs, Link, List } from "@mui/material";
import * as React from "react";
import { AppContext } from "./AppContext";
import FilesBrowserItem from "./FilesBrowserItem";

import directoryItems from "../webdav";

export default function FilesBrowser(props) {
  const [currentItems, setCurrentItems] = React.useState([]);
  const [crumbs, setCrumbs] = React.useState([]);
  const [appContext] = React.useContext(AppContext);

  React.useEffect(() => {
    async function getItems() {
      const items = await directoryItems(appContext.sid);
      setCurrentItems(items);
    }
    getItems();
  }, [appContext.sid]);

  const handleBrowse = async (item) => {
    if (item.type === "directory") {
      console.log(item);
      // filename starts with "/.." for some reason or another
      const path = item.filename.substring(3);
      console.log(`PATH: ${path}`);
      const newCrumbs = path.split("/");
      setCrumbs(newCrumbs);
      // fetch next level and update currentItems
      const items = await directoryItems(appContext.sid, path);
      setCurrentItems(items);
    } else {
      console.log(item.type);
    }
  };

  return (
    <>
      <Breadcrumbs aria-label="breadcrumb">
        {crumbs.map((crumb) => (
          <Link key={crumb}>{crumb}</Link>
        ))}
      </Breadcrumbs>
      <List dense>
        {
          //TODO: use Array.prototype.shift() to pop the first element off the array
          currentItems.map((item) => (
            <FilesBrowserItem
              key={item.filename}
              Item={item}
              OnClick={handleBrowse}
            />
          ))
        }
      </List>
    </>
  );
}
