import React, { Suspense, lazy } from "react";
import SidePanel, { DrawerHeader } from "./SidePanel";
import {
  BrowserRouter as Router,
  Outlet,
  Route,
  Routes,
} from "react-router-dom";
import { AppContext } from "./AppContext";
import { Box, CssBaseline, StyledEngineProvider } from "@mui/material";
import { ThemeProvider } from "@emotion/react";
import { SnackbarProvider } from "notistack";
import theme from "../theme";
import Settings from "./settings";
import Explorer from "../routes/explorer";
import GetSid from "../routes/getsid";
import useLocalStorage from "../hooks/useLocalStorage";

const Roger = lazy(() => import("../routes/roger"));
export default function App() {
  const [appContext, setAppContext] = useLocalStorage("appContext", {
    // these are the vanilla/fallback defaults, used if localStorage is empty or not available
    // TODO: oursource into a defaultAppContext object in AppContext.js
    sid: "",
    textgridServiceClass: 0,
    plugins: [],
  });

  return (
    <AppContext.Provider value={[appContext, setAppContext]}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <SnackbarProvider>
            <Router>
              <Suspense fallback={<div>Loading...</div>}>
                <Routes>
                  <Route
                    path="/"
                    element={
                      <Box sx={{ display: "flex" }}>
                        <Box component="nav">
                          <SidePanel />
                        </Box>
                        <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                          <DrawerHeader />
                          <Outlet />
                        </Box>
                      </Box>
                    }
                  >
                    <Route index element={<Settings />} />
                    <Route path="explorer" element={<Explorer />} />
                    <Route path="settings" element={<Settings />} />
                    <Route path="getsid" element={<GetSid />} />
                    <Route path="roger" element={<Roger />} />
                  </Route>
                </Routes>
              </Suspense>
            </Router>
          </SnackbarProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </AppContext.Provider>
  );
}
