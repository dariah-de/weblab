import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import DescriptionIcon from "@mui/icons-material/Description";
import FolderIcon from "@mui/icons-material/Folder";
import * as React from "react";

export default function FilesBrowserItem(props) {
  const item = props.Item;
  const onClickHandler = props.OnClick;

  return (
    <ListItemButton onClick={() => onClickHandler(item)}>
      <ListItemIcon>
        {item.type === "directory" ? <FolderIcon /> : <DescriptionIcon />}
      </ListItemIcon>
      <ListItemText
        primary={item.basename}
        secondary={
          <>
            lastmod: {item.lastmod},
            {item.type === "file"
              ? ` size: ${item.size}, mime: ${item.mime}`
              : ""}
          </>
        }
      />
    </ListItemButton>
  );
}
