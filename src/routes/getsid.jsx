import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useSnackbar } from "notistack";
import { AppContext } from "../components/AppContext";

export default function GetSid() {
  // the context is not used here, only set => get only the setter
  const setAppContext = React.useContext(AppContext)[1];
  const location = useLocation();
  const navigate = useNavigate();

  const { enqueueSnackbar } = useSnackbar();

  React.useEffect(() => {
    if (location.hash) {
      // strip '#' and set sid
      setAppContext((context) => ({
        ...context,
        sid: location.hash.substring(1),
      }));
      enqueueSnackbar("Obtained a Textgrid Session ID.", {
        variant: "success",
      });
    } else {
      enqueueSnackbar("Obtaining a Textgrid Session ID failed.", {
        variant: "error",
      });
    }
    navigate("/", { replace: true });
  }, [enqueueSnackbar, location, navigate, setAppContext]);

  return null;
}
